from django.shortcuts import render, get_object_or_404
from todos.models import TodoList


# Create your views here.
def todo_list_list(request):
    todo_listy = TodoList.objects.all()
    context = {
        "todo_listy": todo_listy,
    }
    return render(request, "todos/todos.html", context)


def todo_details(request, id):
    details = get_object_or_404(TodoList, id=id)
    context = {
        "details": details
    }
    return render(request, "todos/details.html", context)
